"""
Test Client for individual classes in projects
"""

from projects import xubuntu_releases
import json  # import json to read project info stored in json file
   
# main function
if __name__ =="__main__":
    with open("data.json", "r", encoding="utf-8") as file:
        data = json.load(file)
        print(xubuntu_releases.check(data, "xubuntu_releases"))
        