"""
Contains abstract class for a mirrored project
"""

from abc import ABC
import re

import requests

from shared import CSC_MIRROR


class Project(ABC):
	"""Abstract class for a mirrored project"""

	@staticmethod
	def check(data, project, current_time):
		"""Check if project packages are up-to-date"""
		csc_url = CSC_MIRROR + data[project]["csc"] + data[project]["file"]
		upstream_url = data[project]["upstream"] + data[project]["file"]

		req = requests.get(csc_url)
		req.raise_for_status()
		CSC = req.text

		req = requests.get(upstream_url)
		req.raise_for_status()
		upstream = req.text

		if upstream == CSC:
			return True

		try:
			return int(upstream) - int(CSC) < data[project]["out_of_sync_interval"]
		except ValueError:
			return False
