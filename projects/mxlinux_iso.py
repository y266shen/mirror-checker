"""
Contains mxlinux_iso class
"""

import os
from project import Project
from shared import CSC_MIRROR
import requests
import datefinder # another date finding library
from datetime import timedelta
import re
import pandas as pd

class mxlinux_iso(Project):
    """mxlinux_iso class"""
    # this method is to check the mirror tracker, but unfortunately, the mirror tracker is behaving a bit strange so we check with other mirrors
    """@staticmethod
    def check(data, project, current_time):
        page = requests.get(data[project]["upstream"]).text
        indexOfFile = page.find("mirror.csclub.uwaterloo.ca")

        m = re.search(r'(\d+ hour)|(\d+ hours)|(\d+(\.)?\d+ days)', page[indexOfFile:]) # solution from: https://stackoverflow.com/questions/21074100/how-to-convert-standard-timedelta-string-to-timedelta-object/21074460

        duration = pd.to_timedelta(m.group(0))
        
        return duration <= pd.to_timedelta(data[project]["out_of_sync_interval"], unit='s')"""

    @staticmethod
    def checker(directory_URL, file_name):
        page = requests.get(directory_URL).text
        file_index = page.find(file_name)
        # print(page)

        if file_index == -1:
            return False

        str_dates = re.findall(r'(\d{2}-\w{3}-\d{4} \d{2}:\d{2})|(\d{4}-\w{3}-\d{2} \d{2}:\d{2})|(\d{4}-\d{2}-\d{2} \d{2}:\d{2})', page[file_index:])

        # print(str_dates)
        return list(datefinder.find_dates("".join(str_dates[0])))[0]
    
    @classmethod
    def check_iso(cls, site, mirrors):
        for mirror in mirrors:
            # print(cls.checker(site, "md5sum.txt"))
            # print(cls.checker(mirror, "md5sum.txt"))
            if cls.checker(site, "TIME.txt") < cls.checker(mirror, "TIME.txt"):
                return False
        return True

    @classmethod
    def check(cls, data, project, current_time):
        """Check if project packages are up-to-date"""
        
        csc_url = CSC_MIRROR + data[project]["csc"] + data[project]["file"]
        upstream_url = data[project]["upstream"] + data[project]["file"]
        
        # print(cls.check_iso(upstream_url+"slackware-iso/", csc_url+"slackware-iso/"))
        mirrors = data[project]["mirrors"]
        
        return cls.check_iso(csc_url, mirrors)
