"""
Contains CPAN class
"""

import requests

from project import Project
from shared import CSC_MIRROR


class CPAN(Project):
	"""CPAN class"""

	@staticmethod
	def check(data, project, current_time):
		res_json = requests.get("http://mirrors.cpan.org/cpan-json.txt").json()
		for mirror in res_json:
			if mirror["url"] == f"{CSC_MIRROR}CPAN/":
				data[project]["out_of_sync_since"] = int(mirror["age"])
				return current_time - data[project]["out_of_sync_since"] <= data[project]["out_of_sync_interval"]
		return False
