"""
Contains GentooPortage class
"""

import os

from project import Project

import requests
import datefinder # another date finding library
from datetime import timedelta
import re
import pandas as pd


class GentooPortage(Project):
	"""GentooPortage class"""

	@staticmethod
	def check(data, project, current_time):
		"""rsync_command = "rsync -q {}{} {}"
		os.system(rsync_command.format(data[project]["csc"],
		                               data[project]["file"],
		                               "csc_manifest"))
		os.system(rsync_command.format(data[project]["upstream1"],
		                               data[project]["file"],
		                               "upstream_manifest1"))
		os.system(rsync_command.format(data[project]["upstream2"],
		                               data[project]["file"],
		                               "upstream_manifest2"))
		stream1 = os.popen("diff csc_manifest upstream_manifest1")
		output1 = stream1.read()
		stream2 = os.popen("diff csc_manifest upstream_manifest2")
		output2 = stream2.read()
		os.system("rm csc_manifest")
		os.system("rm upstream_manifest1")
		os.system("rm upstream_manifest2")
		return 0 in [len(output1), len(output2)]"""

		# i'm changing the above code to the bottom one, since the above one only works in linux
		page = requests.get(data[project]["upstream"]).text
		indexOfFile = page.find("rsync4.ca.gentoo.org")
		
		m = re.search(r'(\d+ minutes?)|(\d+ hours?)|(\d+(\.)?\d+ days?)', page[indexOfFile:])
		
		duration = pd.to_timedelta(m.group(0))
		data[project]["out_of_sync_since"] = current_time - duration.total_seconds()
		
		return duration <= pd.to_timedelta(data[project]["out_of_sync_interval"], unit='s')
