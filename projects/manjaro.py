"""
Contains manjaro class
"""

import os
from project import Project
from shared import CSC_MIRROR
import requests
import datefinder # another date finding library
from datetime import timedelta
import re
import pandas as pd

class manjaro(Project):
    """manjaro class"""
    @staticmethod
    def check(data, project, current_time):
        page = requests.get(data[project]["upstream"]).text
        indexOfFile = page.find("mirror.csclub.uwaterloo.ca/manjaro")

        m = re.search(r'(?P<hours>\d+):(?P<minutes>\d+)', page[indexOfFile:]) # solution from: https://stackoverflow.com/questions/21074100/how-to-convert-standard-timedelta-string-to-timedelta-object/21074460
        duration = timedelta(**{key: float(val) for key, val in m.groupdict().items()})
        data[project]["out_of_sync_since"] = current_time - duration.total_seconds()

        return duration <= pd.to_timedelta(data[project]["out_of_sync_interval"], unit='s')
        
        # https://launchpad.net/ubuntu/+mirror/mirror.csclub.uwaterloo.ca-archive
