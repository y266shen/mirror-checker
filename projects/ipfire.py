"""
Contains IPFire class
"""

import requests

from project import Project


class IPFire(Project):
	"""IPFire class"""

	@staticmethod
	def check(data, project, current_time):
		ipfire_url = "https://mirrors.ipfire.org/mirrors/mirror.csclub.uwaterloo.ca"
		ipfire_text = requests.get(ipfire_url).text
		return ipfire_text.find("The mirror is up") != -1
