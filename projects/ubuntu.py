"""
Contains ubuntu class
"""

import os
from project import Project
from shared import CSC_MIRROR
import requests
import datefinder # another date finding library
from datetime import timedelta
from datetime import datetime
import re
import pandas as pd

class ubuntu(Project):
    """ubuntu class"""
    @staticmethod
    def check(data, project, current_time):
        page = requests.get(data[project]["upstream"]).text
        return page.count("Up to date") == 21
