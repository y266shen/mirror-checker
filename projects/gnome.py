"""
Contains GNOME class
"""

import re

import requests

from project import Project
from shared import CSC_MIRROR


class GNOME(Project):
	"""GNOME class"""

	@staticmethod
	def check(data, project, current_time):
		file = data[project]["file1"]
		csc_versions = requests.get(CSC_MIRROR + data[project]["csc"] + file).text
		upstream_versions = requests.get(data[project]["upstream1"] + file).text
		csc_latest = re.findall(r"\"\d+\.?\d*", csc_versions)[-1].lstrip('"')
		upstream_latest = re.findall(r"\"\d+\.?\d*", upstream_versions)[-1].lstrip('"')
		if csc_latest != upstream_latest:
			return False
		file += csc_latest + "/"
		csc_versions = requests.get(CSC_MIRROR + data[project]["csc"] + file).text
		upstream_versions = requests.get(data[project]["upstream1"] + file).text
		csc_latest = re.findall(r"\"\d+\.?\w*\.?\w*", csc_versions)[-1].lstrip('"')
		upstream_latest = re.findall(r"\"\d+\.?\w*\.?\w*", upstream_versions)[-1].lstrip('"')
		if csc_latest != upstream_latest:
			return False
		file += csc_latest + "/"
		csc_text = requests.get(CSC_MIRROR + data[project]["csc"] + file
		                        + data[project]["file2"]).text
		try:
			ret = csc_text == requests.get(data[project]["upstream2"] + file
			                               + data[project]["file2"]).text
		except requests.exceptions.RequestException:
			ret = False
		try:
			return ret or csc_text == requests.get(data[project]["upstream3"] + file
			                                       + data[project]["file2"]).text
		except requests.exceptions.RequestException:
			return False
