"""
Contains Artix class
"""

import re
from datetime import datetime
import requests

from project import Project
from shared import CSC_MIRROR

def _get_date(url):
	req = requests.head(url)
	req.raise_for_status()
	return datetime.strptime(req.headers['Last-Modified'], '%a, %d %b %Y %H:%M:%S %Z')

class Artix(Project):
	"""Artix class"""
	@classmethod
	def check(cls, data, project, current_time):
		csc_url = CSC_MIRROR + data[project]['csc']
		upstream_url = data[project]['upstream']

		req = requests.get(upstream_url)
		req.raise_for_status()
		index = req.text

		repos = re.findall(r'href="(\w+)/"', index);

		outdated_since = None
		for repo in repos:
			# Good enough for now, we can change it if Artix gets more arch in the future
			db_path = repo + '/os/x86_64/' + repo + '.db'
			upstream_date = _get_date(data[project]['upstream'] + db_path)
			csc_date = _get_date(CSC_MIRROR + data[project]['csc'] + db_path)
			if csc_date < upstream_date:
				if outdated_since is None or upstream_date < outdated_since:
					outdated_since = upstream_date

		if outdated_since is not None:
			data[project]['out_of_sync_since'] = int(outdated_since.timestamp())
			return current_time - data[project]['out_of_sync_since'] < data[project]['out_of_sync_interval']

		return True
