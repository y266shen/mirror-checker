import requests
import datefinder  # another date finding library
import re
from datetime import datetime
from project import Project
from shared import CSC_MIRROR


class saltstack(Project):
	"""saltstack class"""

	@staticmethod
	def check(data, project, current_time):
		csc_url = CSC_MIRROR + data[project]["csc"] + data[project]["file"]
		upstream_url = data[project]["upstream"] + data[project]["file"]

		page1 = requests.get(csc_url).text
		page2 = requests.get(upstream_url).text

		CSC_release = re.search(r'Latest release: (\d)+.(\d)+ \((.+)\)', page1)
		upstream_release = re.search(r'Latest release: (\d)+.(\d)+ \((.+)\)', page2)

		# print(CSC_release.group(0))
		# print(upstream_release.group(0))

		return CSC_release.group(0) == upstream_release.group(0)
