import requests
import datefinder  # another date finding library
import re
from datetime import datetime
from project import Project
from shared import CSC_MIRROR


class sage(Project):
	"""sagemath class"""

	@staticmethod
	def get_latest_date(dates):
		dates = [list(datefinder.find_dates(date))[0] for date in dates]
		return max(dates)

	@classmethod
	def check(cls, data, project, current_time):
		page1 = requests.get(CSC_MIRROR + data[project]["csc"] + data[project]["file"]).text
		page2 = requests.get("http://mirrors.mit.edu/sage/src/index.html").text
		page3 = requests.get("https://mirror.rcg.sfu.ca/mirror/sage/src/index.html").text

		CSC_dates = re.findall(r'(\d{4}-\d{2}-\d{2} \d{2}:\d{2})', page1)
		MIT_dates = re.findall(r'(\d{4}-\d{2}-\d{2} \d{2}:\d{2})', page2)
		SFU_dates = re.findall(r'(\d{4}-\d{2}-\d{2} \d{2}:\d{2})', page3)

		# print(len(CSC_dates))
		# print(len(MIT_dates))
		# print(len(SFU_dates))
		# print(cls.get_latest_date(CSC_dates))
		# print(cls.get_latest_date(MIT_dates))
		# print(cls.get_latest_date(SFU_dates))

		if len(CSC_dates) < max([len(MIT_dates), len(SFU_dates)]):
			return False
		elif len(CSC_dates) > max([len(MIT_dates), len(SFU_dates)]):
			# if we have more entries than their mirror, ours must be the new one
			#  since distros only add new versions, and don't delete old versions
			return True
		if cls.get_latest_date(CSC_dates) < max([cls.get_latest_date(MIT_dates), cls.get_latest_date(SFU_dates)]):
			return False
		return True
