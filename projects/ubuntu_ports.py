from bs4 import BeautifulSoup
import requests
from project import Project
from shared import CSC_MIRROR

class ubuntu_ports(Project):
    """ubuntu_ports class"""

    @staticmethod
    def scrape(site1, site2):
        # getting the request from url
        r1 = requests.get(site1)
        r2 = requests.get(site2)
        
        # converting the text
        s1 = BeautifulSoup(r1.text,"html.parser")
        s2 = BeautifulSoup(r2.text,"html.parser")

        hrefs1 = [i.attrs['href'] for i in s1.find_all("a")]
        hrefs2 = [i.attrs['href'] for i in s2.find_all("a")]

        for href in hrefs1: # for a href directories
            if href.endswith("/") and href != "../" and href != "/" and not href.startswith("/"):
                # print(href)
                if href not in hrefs2:
                    return False
                elif requests.get(site1+href+"Release").text != requests.get(site2+href+"Release").text:
                    return False
        return True

    @classmethod
    def check(cls, data, project, current_time):
        """Check if project packages are up-to-date"""
        
        csc_url = CSC_MIRROR + data[project]["csc"] + data[project]["file"]
        upstream_url = data[project]["upstream"] + data[project]["file"]
        
        # calling function
        return cls.scrape(upstream_url, csc_url)
