"""
Contains ubuntu_releases class
"""

import os
from project import Project
from shared import CSC_MIRROR
import requests
import datefinder # another date finding library
from datetime import timedelta
from datetime import datetime
import re
import pandas as pd

class ubuntu_releases(Project):
    """ubuntu_releases class"""
    @staticmethod
    def check(data, project, current_time):
        page = requests.get(data[project]["upstream"]).text
        indexOfFile = page.find("last verified")
        matches = list(datefinder.find_dates(page[indexOfFile:]))
        date = matches[0].replace(tzinfo=None) # date is of type datetime.datetime
        data[project]["out_of_sync_since"] = date.timestamp()
        return(pd.to_datetime(current_time, unit='s') - date <= pd.to_timedelta(data[project]["out_of_sync_interval"], unit='s'))
    
        # https://launchpad.net/ubuntu/+mirror/mirror.csclub.uwaterloo.ca-release
